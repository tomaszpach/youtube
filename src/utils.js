const windowLocationHref = window.location.href;
let player;

/* Load and initialize video with default parameters  */
export function loadYtVideo(that) {
    that.state.loadYT.then((YT) => {
        player = new YT.Player(that.youtubePlayerAnchor, {
            width: 600,
            height: 400,
            videoId: that.state.videoID,
            playerVars: {
                autoplay: 1,
                rel: 0,
                start: that.state.seekTo
            },
            events: {
                onReady: setTimeout(() => {
                    that.initialize(player);
                }, 1000)
            }
        });
    })
}

export function loadVideoById(that) {
    if (that.state.videoID !== '') {
        loadYtVideo(that);
    } else alert('please add video id in input')
}

export function getVideoDuration(that) {
    that.setState({videoDuration: that.state.player.getDuration()});
}

export function getVideoCurrentTime(that) {
    that.setState({
        videoCurrentTime: that.state.player.getCurrentTime()
    });
}

export function formatTime(time) {
    time = Math.round(time);

    let minutes = Math.floor(time / 60),
        seconds = time - minutes * 60;

    seconds = seconds < 10 ? '0' + seconds : seconds;

    return minutes + ":" + seconds;
}

export function progressBarChange(value, that) {
    let newTime = that.state.player.getDuration() * (value / that.state.player.getDuration());
    that.state.player.seekTo(newTime)
}

export function play(player) {
    player.playVideo();
}

export function pause(player) {
    player.pauseVideo();
}

/* Add sharable URL to browser bar and update Shareable URL state */
export function addShareUrlToBar(that) {
    let currentTimeMathFloor = Math.floor(that.state.videoCurrentTime);
    window.history.replaceState(null, null, `/?videoID=${that.state.videoID}&t=${currentTimeMathFloor}`);

    updateShareableUrlState(that);
}

export function updateShareableUrlState(that) {
    that.setState({
        sharableUrl: window.location.href
    })
}

/* Regex for time (/t=([^&]+)/) || Regex for video ID (/videoID=([^&]+)/ */
export function getVideoDetailsFromUrl(regex) {
    return regex.exec(windowLocationHref) ? regex.exec(windowLocationHref)[1] : null;
}