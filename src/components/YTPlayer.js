import React, {Component} from 'react'
import InputRange from "react-input-range";
import 'react-input-range/lib/css/index.css';
import {CopyToClipboard} from "react-copy-to-clipboard";
import {
    addShareUrlToBar,
    formatTime,
    getVideoCurrentTime, getVideoDetailsFromUrl,
    getVideoDuration, pause,
    play, progressBarChange, loadYtVideo, loadVideoById
} from "../utils";

let loadYT;

export class YTPlayer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadYT: null,
            player: null, // Global object with player. used to manipulate YT player
            videoDuration: 0, // video duration in seconds
            videoCurrentTime: 0, // video current time in seconds
            seekTo: getVideoDetailsFromUrl(/t=([^&]+)/) || 0, // it's a time where video starts to play
            videoID: getVideoDetailsFromUrl(/videoID=([^&]+)/) || '', // loaded video id
            sharableUrl: ''
        };
    }

    componentDidMount() {
        if (!loadYT) {
            loadYT = new Promise((resolve) => {
                const tag = document.createElement('script');
                tag.src = 'https://www.youtube.com/iframe_api';
                const firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                window.onYouTubeIframeAPIReady = () => resolve(window.YT);
            });
        }
        loadYT.then(() => {
            this.setState({
                loadYT
            }, () => {
                if (this.state.videoID !== '') {
                    loadYtVideo(this)
                }
            })
        });
    }

    initialize(player) {
        // Clear any old interval.
        clearInterval(this.interval);

        // Start interval to update elapsed time display and
        // the elapsed part of the progress bar every second.
        this.interval = setInterval(() => getVideoCurrentTime(this), 100);

        this.setState({
            player
        });

        getVideoDuration(this);
    }

    render() {
        const marginLeft = 10,
            marginBottom = 15;

        return (
            <section className='youtubeComponent-wrapper'>
                <form className='add-video'
                      style={{marginBottom}}
                      onSubmit={e => {
                          e.preventDefault();
                          loadVideoById(this)
                      }}
                >
                    <input type="text" name="videoID" value={this.state.videoID}
                           onChange={event => this.setState({videoID: event.target.value})}/>
                    <button type="submit" style={{marginLeft}}>Add video</button>
                </form>
                <div ref={(r) => {
                    this.youtubePlayerAnchor = r
                }}/>

                {this.state.videoDuration !== 0 ? (
                    <div>
                        <div
                            style={{marginBottom}}>Duration <span>{formatTime(this.state.videoCurrentTime)} / {formatTime(this.state.videoDuration)}</span>
                        </div>
                        <button onClick={() => play(this.state.player)}>play</button>
                        <button onClick={() => pause(this.state.player)} style={{marginBottom, marginLeft}}>pause
                        </button>
                        <InputRange
                            maxValue={this.state.videoDuration}
                            minValue={0}
                            value={Math.round(this.state.videoCurrentTime)}
                            onChange={value => progressBarChange(value, this)}/>

                        <button onClick={() => addShareUrlToBar(this)} style={{marginBottom}}>Share
                        </button>
                        {this.state.sharableUrl !== '' ? (
                            <div>
                                <input value={this.state.sharableUrl}
                                       onChange={() => this.setState({copied: false})}/>

                                <CopyToClipboard text={this.state.sharableUrl}
                                                 onCopy={() => this.setState({copied: true})}>
                                    <button style={{marginLeft, marginBottom}}>Copy</button>
                                </CopyToClipboard>

                                {this.state.copied ? <div style={{color: 'red'}}>Copied to clipboard.</div> : null}
                            </div>
                        ) : null}
                    </div>
                ) : null}
            </section>
        )
    }
}

export default YTPlayer;