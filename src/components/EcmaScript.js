import React, {Component} from 'react'

export class EcmaScript extends Component {
    constructor(props) {
        super(props);
        this.state = {
            test: 0
        };
    }

    componentDidMount() {
        [1, 12, 123, 1234, 12345].map(num =>
            console.log(num.toString().padStart(10, '0'))
        );

        const promise = new Promise((resolve, reject) => {
            setTimeout(resolve, 100, 'foo');
        });
        promise.then((result) => { console.log(result) })
    }

    render() {
        return (
            <div>
                <div>{'heart'.padStart(10, "❤️")}</div>
            </div>
        )
    }
}

export default EcmaScript;